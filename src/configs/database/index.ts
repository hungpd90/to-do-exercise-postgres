import { Sequelize } from 'sequelize';
import dotenv from 'dotenv';

dotenv.config({ path: './../config.env' });

const { DB_NAME, DB_USERNAME, DB_HOST, PORT } = process.env;
const DB_PORT: any = process.env.DB_PORT || 5432;
const DB_PASSWORD = process.env.DB_PASSWORD || '';

const db: { sequelize?: Sequelize } = {};

const sequelize = new Sequelize(
  DB_NAME as string,
  DB_USERNAME as string,
  DB_PASSWORD as string,
  {
    host: DB_HOST,
    port: DB_PORT || 5432,
    dialect: 'postgres',
  }
);

db.sequelize = sequelize;

export default db;
