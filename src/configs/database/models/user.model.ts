import { DataTypes, Model, ModelStatic, Sequelize } from "sequelize";
import db from "..";

export interface IUser extends Model {
  username: string;
  password: string;
  email: string;
}

const User = db.sequelize?.define(
  "user",
  {
    username: {
      type: DataTypes.STRING,
      primaryKey: true,
      allowNull: false,
      unique: true,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
  },
  {}
) as ModelStatic<IUser>;

export default User;
