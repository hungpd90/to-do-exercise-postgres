import Task from './task.model';
import User from './user.model';

const models: { associate?: any } = {};

const associate = async (): Promise<void> => {
  await Task.sync();
  await User.sync();
};

models.associate = associate;

export default models;
