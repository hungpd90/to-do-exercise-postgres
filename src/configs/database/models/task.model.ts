import { string } from "joi";
import { DataTypes, Model, ModelStatic } from "sequelize";
import db from "..";

export interface ITask extends Model {
  name: string;
}

const Task = db.sequelize?.define<ITask>(
  "Task",
  {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {}
) as ModelStatic<ITask>;

export default Task;
