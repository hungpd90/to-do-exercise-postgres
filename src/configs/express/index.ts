import express from 'express';
import dotenv from 'dotenv';
import routes from './../../api/v1/modules';
import cookieParser from 'cookie-parser';

dotenv.config({ path: './config.env' });
const app = express();
app.use(express.json());
app.use(cookieParser());
app.use(
  (req: express.Request, res: express.Response, next: express.NextFunction) => {
    console.log('REQ: ' + req.body);
    next();
  }
);
app.use('/v1', routes);

export default app;
