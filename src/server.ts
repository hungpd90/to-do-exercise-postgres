import db from './configs/database';
import models from './configs/database/models';
import app from './configs/express/index';

const PORT = process.env.PORT || 9000;

app.listen(PORT, async () => {
  try {
    await db.sequelize?.sync();
    await models.associate();
    console.log('Database connected!');
    console.log(`🚀 Server listening on port: ${PORT}`);
  } catch (error) {
    console.log('Failed to start server!');
    console.log(error);
  }
});
