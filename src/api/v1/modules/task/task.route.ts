import { Router } from 'express';
import TaskController from './task.controller';
import AuthController from '../auth/auth.controller';
import Task from './../../../../configs/database/models/task.model';
import TaskService from './task.services';

const taskService = new TaskService(Task);
const taskController = new TaskController(taskService);

const TaskRoute = Router();

TaskRoute.get('/', taskController.getAllTasks);
TaskRoute.post('/', taskController.createTask);

export default TaskRoute;
