import TaskService from './task.services';
import { Request, Response, NextFunction } from 'express';
import { ITask } from './../../../../configs/database/models/task.model';

export default class TaskController {
  private service: TaskService;
  constructor(service: TaskService) {
    this.service = service;
  }

  public getAllTasks = async <TaskController>(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<Response> => {
    try {
      const tasks: ITask[] = await this.service.getAllTasks();
      return res.status(200).json({
        status: 'success',
        length: tasks.length,
        data: {
          tasks,
        },
      });
    } catch (err) {
      return res.status(400).json({
        status: 'error',
        message: err,
      });
    }
  };

  public getTask = async <TaskController>(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<Response> => {
    try {
      const task: ITask = await this.service.getTask(req.params.name);
      return res.status(200).json({
        status: 'success',
        data: {
          task,
        },
      });
    } catch (err) {
      return res.status(400).json({
        status: 'error',
        message: err,
      });
    }
  };

  public createTask = async <TaskController>(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<Response> => {
    try {
      const task: ITask = await this.service.createTask(req.body);
      return res.status(200).json({
        status: 'success',
        data: {
          task,
        },
      });
    } catch (err) {
      console.log(err);
      return res.status(400).json({
        status: 'error',
        message: err,
      });
    }
  };

  public deleteTask = async <TaskController>(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<Response> => {
    try {
      await this.service.deleteTask(req.params.name);
      return res.status(200).json({
        status: 'success',
        data: null,
      });
    } catch (err) {
      return res.status(400).json({
        status: 'error',
        message: err,
      });
    }
  };
}
