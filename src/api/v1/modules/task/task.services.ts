import { ModelStatic } from 'sequelize';
import dotenv from 'dotenv';
import { ITask } from './../../../../configs/database/models/task.model';

dotenv.config({ path: './config.env' });

export default class TaskService {
  private readonly model: ModelStatic<ITask>;
  constructor(model: ModelStatic<ITask>) {
    this.model = model;
  }
  public async createTask(body: ITask): Promise<ITask> {
    try {
      const task: ITask = await this.model.create({
        name: body.name,
      });
      console.log(body.name);
      if (!task) {
        throw 'Cannot create task';
      }
      return task;
    } catch (err) {
      throw err;
    }
  }

  public async deleteTask(taskName: string): Promise<void> {
    try {
      await this.model.destroy({ where: { name: taskName } });
    } catch (err) {
      throw err;
    }
  }

  public async getAllTasks(): Promise<ITask[]> {
    try {
      const tasks: ITask[] | null = await this.model.findAll();
      if (!tasks) {
        throw 'Cannot get tasks';
      }
      return tasks;
    } catch (err) {
      throw err;
    }
  }

  public async getTask(taskName: string): Promise<ITask> {
    try {
      const task: ITask | null = await this.model.findOne({
        where: { name: taskName },
      });
      if (!task) {
        throw 'Cannot get task';
      }
      return task;
    } catch (err) {
      throw err;
    }
  }
}
