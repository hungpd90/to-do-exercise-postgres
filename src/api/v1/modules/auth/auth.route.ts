import { Router } from 'express';
import AuthController from './auth.controller';
import JoiSchema from '../user/user.schema';
import { commonValidator } from './../../middlewares/validate.middleware';
import UserService from './../user/user.services';
import User from './../../../../configs/database/models/user.model';

const userService = new UserService(User);
const authController = new AuthController(userService);
const joiSchema = new JoiSchema();
const router = Router();

router
  .route('/login')
  .post(commonValidator(joiSchema.loginSchema), authController.login);

router
  .route('/register')
  .post(commonValidator(joiSchema.registerSchema), authController.register);

router.route('/logout').get(authController.logout);

export default router;
