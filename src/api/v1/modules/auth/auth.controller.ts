import UserService from '../user/user.services';
import { Request, Response, NextFunction } from 'express';
import dotenv from 'dotenv';
import jwt from 'jsonwebtoken';

dotenv.config({ path: './config.env' });

const JWT_SECRET_KEY = process.env.JWT_SECRET_KEY as string;

const JWT_EXPIRED_IN_MINUTES = process.env.JWT_EXPIRED_IN_MINUTES;
const JWT_COOKIE_EXPIRED_IN_DAYS: any = process.env.JWT_COOKIE_EXPIRED_IN_DAYS;
if (
  JWT_EXPIRED_IN_MINUTES === undefined &&
  JWT_COOKIE_EXPIRED_IN_DAYS === undefined
) {
  throw 'Cannot read variable';
}

export default class AuthController {
  public service: UserService;

  constructor(service: UserService) {
    this.service = service;
  }

  public signToken = (username: string) => {
    console.log(
      'JWT_EXPIRED_IN_MINUTES:================================================ ' +
        JWT_EXPIRED_IN_MINUTES
    );
    return jwt.sign({ username }, JWT_SECRET_KEY, {
      expiresIn: JWT_EXPIRED_IN_MINUTES,
    });
  };

  public createSendToken = (user: any, statusCode: number, res: Response) => {
    const token = this.signToken(user.username);
    res.cookie('jwt', token, {
      expires: new Date(
        Date.now() + JWT_COOKIE_EXPIRED_IN_DAYS * 24 * 60 * 60 * 1000
      ),
      httpOnly: true,
    });

    res.status(statusCode).json({
      status: 'success',
      data: {
        user,
      },
    });
  };

  public register = async <AuthController>(
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    try {
      const user = await this.service.createUser(req.body);
      this.createSendToken(user, 200, res);
    } catch (err) {
      res.status(400).json({
        status: 'error',
        message: err,
      });
    }
  };

  public login = async <AuthController>(
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    try {
      const user = await this.service.getUserByUsername(req.body.username);
      if (!user) {
        throw 'Username or password not correct';
      }
      const check = await this.service.correctPassword(
        req.body.password,
        user.password
      );
      if (!check) {
        throw 'Username or password not correct';
      }
      this.createSendToken(user, 200, res);
    } catch (err) {
      res.status(400).json({
        status: 'error',
        message: err,
      });
    }
  };

  public logout = <UserController>(
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    res.cookie('jwt', 'loggedout', {
      expires: new Date(Date.now() + 10 * 1000),
      httpOnly: true,
    });
    res.status(200).json({ status: 'success' });
  };

  public isAuth = async <UserController>(
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    try {
      let token;
      if (
        req.headers.authorization &&
        req.headers.authorization.startsWith('Bearer')
      ) {
        token = req.headers.authorization.split(' ')[1];
      } else if (req.cookies) {
        token = req.cookies.jwt;
      }
      if (!token || token == 'loggedout') {
        throw 'Not logged in!';
      }

      console.log(token);
      const decoded: any = await jwt.verify(token, JWT_SECRET_KEY);
      const user = this.service.getUserByUsername(decoded.username);
      if (!user) {
        throw 'User of this token not exist!';
      }

      (<any>req).user = user;
      next();
    } catch (err) {
      console.log(err);
      res.status(401).json({
        status: 'error',
        message: err,
      });
    }
  };
}
