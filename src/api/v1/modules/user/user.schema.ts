import joi from "joi";
function password(value: string, helpers: any) {
  if (value.length < 6) {
    return helpers.message("password must be at least 6 characters");
  }
  if (value.search(/\s/) != -1) {
    return helpers.message("password cannot contain spaces");
  }
  if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
    return helpers.message(
      "password must contain at least 1 letter or 1 number"
    );
  }
  return value;
}
function username(value: string, helpers: any) {
  if (value.search(/\s/) != -1) {
    return helpers.message("username cannot contain spaces");
  }
  if (!value.match(/[a-zA-Z0-9]/)) {
    return helpers.message("please enter another username");
  }
}
export default class JoiSchema {
  public registerSchema: joi.ObjectSchema = joi.object({
    username: joi.string().required().min(6).max(40).custom(username),
    password: joi.string().required().min(6).max(40).custom(password),
    email: joi.string().required().email(),
  });
  public loginSchema: joi.ObjectSchema = joi.object({
    username: joi.string().required().min(6).max(40).custom(username),
    password: joi.string().required().min(6).max(40).custom(password),
  });
}
