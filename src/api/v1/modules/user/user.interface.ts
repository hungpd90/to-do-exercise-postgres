export interface IUser {
  username: string;
  email: string;
  password: string;
}
export interface IGetUserResponse {
  username: string;
  email: string;
}
