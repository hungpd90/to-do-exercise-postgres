import { ModelStatic } from 'sequelize';
import { IUser } from './../../../../configs/database/models/user.model';
import bcrypt from 'bcryptjs';

export default class UserService {
  private readonly model: ModelStatic<IUser>;

  constructor(model: ModelStatic<IUser>) {
    this.model = model;
  }

  async createUser(body: IUser): Promise<IUser> {
    try {
      const user: IUser = await this.model.create({
        username: body.username,
        password: await this.hashPassword(body.password),
        email: body.email,
      });
      if (!user) {
        throw 'Cannot create user';
      }
      return user;
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  async getUserByUsername(userName: string): Promise<IUser> {
    try {
      const user: IUser | null = await this.model.findOne({
        where: { username: userName },
      });
      if (!user) {
        throw 'Cannot get user';
      }
      return user;
    } catch (err) {
      throw err;
    }
  }
  async getAllUsers(): Promise<IUser[]> {
    try {
      const users: IUser[] = await this.model.findAll(); //query("SELECT * FROM users");
      if (!users) {
        throw 'Cannot get users';
      }
      return users;
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  async hashPassword(password: string): Promise<string> {
    try {
      const hashedPassword: string = await bcrypt.hash(password, 12);
      return hashedPassword;
    } catch (err) {
      throw err;
    }
  }

  async correctPassword(
    cadidatePassword: string,
    userPassword: string
  ): Promise<boolean> {
    try {
      return await bcrypt.compare(cadidatePassword, userPassword);
    } catch (err) {
      throw err;
    }
  }
}
