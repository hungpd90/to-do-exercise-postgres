import { Router } from 'express';
import UserController from './user.controller';
import UserService from './user.services';
import User from './../../../../configs/database/models/user.model';

const userService = new UserService(User);
const userController = new UserController(userService);

const UserRoute = Router();

UserRoute.route('/')
  .post(userController.createUser)
  .get(userController.getAllUsers);

UserRoute.route('/:username').get(userController.getAnUser);

export default UserRoute;
