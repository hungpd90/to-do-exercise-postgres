import { Request, Response, NextFunction } from 'express';
import UserService from './user.services';
import { serializeUser } from './user.serializer';
import { IUser } from './user.interface';
export default class UserController {
  private service: UserService;

  constructor(service: UserService) {
    this.service = service;
  }

  public createUser = async <UserController>(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<Response> => {
    try {
      const data = await this.service.createUser(req.body);
      return res.status(200).json({
        status: 'success',
        data: serializeUser(data),
      });
    } catch (err) {
      return res.status(400).json({
        status: 'error',
        message: err,
      });
    }
  };

  public getAllUsers = async <UserController>(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<Response> => {
    try {
      const data = await this.service.getAllUsers();
      return res.status(200).json({
        status: 'success',
        length: data.length,
        data: data.map((el: IUser) => serializeUser(el)),
      });
    } catch (err) {
      return res.status(400).json({
        status: 'error',
        message: err,
      });
    }
  };

  public getAnUser = async <UserController>(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<Response> => {
    try {
      const data = await this.service.getUserByUsername(req.params.username);
      return res.status(200).json({
        status: 'success',
        data: serializeUser(data),
      });
    } catch (err) {
      return res.status(400).json({
        status: 'error',
        message: err,
      });
    }
  };
}
