import { IUser, IGetUserResponse } from './user.interface';

export function serializeUser(model: IUser): IGetUserResponse {
  if (!model) {
  }

  return {
    username: model.username,
    email: model.email,
  };
}
