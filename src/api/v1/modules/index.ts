import UserRoute from './user/user.route';
import AuthRoute from './auth/auth.route';
import TaskRoute from './task/task.route';
import { Router } from 'express';

const router = Router();

router.use('/users', UserRoute);
router.use('/auth', AuthRoute);
router.use('/tasks', TaskRoute);

export default router;
